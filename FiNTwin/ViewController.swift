//
//  ViewController.swift
//  FiNTwin
//
//  Created by Tu Doan on 25/04/2021.
//

import UIKit

import AVFoundation
import CoreMedia

class ViewController: UIViewController {
    let mainGroup = UIStackView()
    let imageView = UIImageView(frame: .zero)
    let filtersControl = UISegmentedControl(items: FilterNames)
    let captureSession = AVCaptureSession()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupUIForCaptureSession()
        setupCaptureSession()
    }
    
    override func viewDidLayoutSubviews()
    {
        let topMargin = topLayoutGuide.length
        
        mainGroup.frame = CGRect(x: 0, y: topMargin, width: view.frame.width, height: view.frame.height - topMargin).insetBy(dx: 5, dy: 5)
    }
    
    private func setupUIForCaptureSession() {
        view.addSubview(mainGroup)
        mainGroup.axis = .vertical
        mainGroup.distribution = .fill
        
        mainGroup.addArrangedSubview(imageView)
        mainGroup.addArrangedSubview(filtersControl)
        
        imageView.contentMode = .scaleAspectFit
        
        filtersControl.selectedSegmentIndex = 0
    }
    
    private func setupCaptureSession() {
        captureSession.sessionPreset = .photo
        inputCaptureSession(captureSession: captureSession)
        outputCaptureSession(captureSession: captureSession)
    }
    
    private func inputCaptureSession(captureSession: AVCaptureSession) {
        guard let backCamera = AVCaptureDevice.default(for: .video) else {
            return
        }
        
        do
        {
            let input = try AVCaptureDeviceInput(device: backCamera)
            captureSession.addInput(input)
        }
        catch
        {
            print("can't access camera")
            return
        }
    }
    
    private func outputCaptureSession(captureSession: AVCaptureSession) {
        // although we don't use this, it's required to get captureOutput invoked
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//        view.layer.addSublayer(previewLayer)
        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self,
                                            queue: DispatchQueue(label: "sample buffer delegate"))
        
        if captureSession.canAddOutput(videoOutput)
        {
            captureSession.addOutput(videoOutput)
        }
        
        captureSession.startRunning()
    }

}


extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        DispatchQueue.main.async {
            guard let filter = Filters[FilterNames[self.filtersControl.selectedSegmentIndex]] else
            {
                return
            }
            
            let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
            let cameraImage = CIImage(cvPixelBuffer: pixelBuffer!)
            
            filter!.setValue(cameraImage, forKey: kCIInputImageKey)
            
            let filteredImage = UIImage(ciImage: filter!.value(forKey: kCIOutputImageKey) as! CIImage)
            
          
            
            self.imageView.image = filteredImage
        }
    }
    
}
