//
//  AVCaptureSessionUtil.swift
//  FiNTwin
//
//  Created by Tu Doan on 25/04/2021.
//

import UIKit
import AVFoundation
import CoreMedia

let CMYKHalftone = "CMYK Halftone"
let CMYKHalftoneFilter = CIFilter(name: "CICMYKHalftone", parameters: ["inputWidth" : 20, "inputSharpness": 1])
let ComicEffect = "Comic Effect"
let ComicEffectFilter = CIFilter(name: "CIComicEffect")

let Filters = [
    CMYKHalftone: CMYKHalftoneFilter,
    ComicEffect: ComicEffectFilter
]

//let FilterNames = [String](Filters.keys).sort()
let FilterNames: [String] = Filters.keys.sorted()

class AVCaptureSessionUtil {
    private func setupCaptureSession() {
        let captureSession = AVCaptureSession()
        captureSession.sessionPreset = .photo
        inputCaptureSession(captureSession: captureSession)
        outputCaptureSession(captureSession: captureSession)
    }
    
    private func inputCaptureSession(captureSession: AVCaptureSession) {
        guard let backCamera = AVCaptureDevice.default(for: .video) else {
            return
        }
        
        do
        {
            let input = try AVCaptureDeviceInput(device: backCamera)
            captureSession.addInput(input)
        }
        catch
        {
            print("can't access camera")
            return
        }
    }
    
    private func outputCaptureSession(captureSession: AVCaptureSession) {
        // although we don't use this, it's required to get captureOutput invoked
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//        view.layer.addSublayer(previewLayer)
        
        let videoOutput = AVCaptureVideoDataOutput()
//        videoOutput.setSampleBufferDelegate(self,
//                                            queue: DispatchQueue(label: "sample buffer delegate"))
        
        if captureSession.canAddOutput(videoOutput)
        {
            captureSession.addOutput(videoOutput)
        }
        
        captureSession.startRunning()
    }

    func captureOutput(captureOutput: AVCaptureOutput!,
                       didOutputSampleBuffer sampleBuffer: CMSampleBuffer!,
                       fromConnection connection: AVCaptureConnection!)
    {
//        guard let filter = Filters[FilterNames[filtersControl.selectedSegmentIndex]] else
//        {
//            return
//        }
//
//        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
//        let cameraImage = CIImage(cvPixelBuffer: pixelBuffer!)
//
//        filter!.setValue(cameraImage, forKey: kCIInputImageKey)
//
//        let filteredImage = UIImage(ciImage: filter!.value(forKey: kCIOutputImageKey) as! CIImage)
//
//        DispatchQueue.main.async {
//            self.imageView.image = filteredImage
//        }
        
    }
}
